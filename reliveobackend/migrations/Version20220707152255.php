<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220707152255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event ADD streamer_id INT NOT NULL');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA725F432AD FOREIGN KEY (streamer_id) REFERENCES streamer (id)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA725F432AD ON event (streamer_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA725F432AD');
        $this->addSql('DROP INDEX IDX_3BAE0AA725F432AD ON event');
        $this->addSql('ALTER TABLE event DROP streamer_id');
    }
}
