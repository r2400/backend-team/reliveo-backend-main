<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class UserSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher)
    {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @inheritDoc
     */
    #[ArrayShape([KernelEvents::VIEW => "array[]"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => [
                ['hashPassword', EventPriorities::PRE_WRITE]
            ]
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getEntity();
//        error_log(print_r($event, true)."\n" , 3, "./ERRLOG.log");
        if (!$object instanceof User) {
            error_log(print_r("NO", true) . "\n", 3, "./ERRLOG.log");

            return;
        }

        error_log(print_r("YES", true) . "\n", 3, "./ERRLOG.log");

    }

    public function hashPassword(ViewEvent $event)
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $user->getPassword()
        );
	$user->setPassword($hashedPassword);
        
	if ($user->getStreamPassword()) {
	    $hashedStreamPassword = $this->passwordHasher->hashPassword(
                $user,
                $user->getStreamPassword()
            );
            $user->setStreamPassword($hashedStreamPassword);
	}

        $this->entityManager->persist($user);
        $this->entityManager->flush();


//        $requestEvent->getRequestType();
////        error_log(print_r($requestEvent->getRequest()->getUser(), true)."\n" , 3, "./ERRLOG.log");
//        error_log(print_r($requestEvent->getRequest()->isMethod("POST"), true)."\n" , 3, "./ERRLOG.log");
//        error_log(print_r($requestEvent->getRequest()->isMethod("GET"), true)."\n" , 3, "./ERRLOG.log");
//        error_log(print_r($requestEvent->getRequestType(), true)."\n" , 3, "./ERRLOG.log");
    }
}
