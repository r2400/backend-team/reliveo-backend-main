<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\PostVideoController;
use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get","post"
        //"post" => ["security" => "is_granted('ROLE_USER','ROLE_DIFFUSEUR','ROLE_ADMINISTRATEUR')"],
        //"delete" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_USER','ROLE_ADMINISTRATEUR')"],
        //"put" => ["security" => "is_granted('ROLE_ADMINISTRATEUR')"],
        //"patch" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_USER','ROLE_ADMINISTRATEUR')"]
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'title' => 'exact', 'description' => 'partial', 'author' => 'exact', 'event' => 'exact', 'favedBy' => 'partial'])]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'posts')]
    #[ORM\JoinColumn(nullable: false)]
    private $author;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'posts')]
    #[ORM\JoinColumn(nullable: false)]
    private $event;

    #[ORM\Column(type: 'string', length: 255)]
    private $videoUrl;

    #[ORM\Column(type: 'datetime')]
    private $timestampStart;

    #[ORM\Column(type: 'datetime')]
    private $timestampEnd;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $viewnumber;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'favPosts')]
    private $favedBy;

    public function __construct()
    {
        $this->favedBy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getTimestampStart(): ?\DateTimeInterface
    {
        return $this->timestampStart;
    }

    public function setTimestampStart(\DateTimeInterface $timestampStart): self
    {
        $this->timestampStart = $timestampStart;

        return $this;
    }

    public function getTimestampEnd(): ?\DateTimeInterface
    {
        return $this->timestampEnd;
    }

    public function setTimestampEnd(\DateTimeInterface $timestampEnd): self
    {
        $this->timestampEnd = $timestampEnd;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getViewnumber(): ?int
    {
        return $this->viewnumber;
    }

    public function setViewnumber(?int $viewnumber): self
    {
        $this->viewnumber = $viewnumber;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getFavedBy(): Collection
    {
        return $this->favedBy;
    }

    public function addFavedBy(User $favedBy): self
    {
        if (!$this->favedBy->contains($favedBy)) {
            $this->favedBy[] = $favedBy;
            $favedBy->addFavPost($this);
        }

        return $this;
    }

    public function removeFavedBy(User $favedBy): self
    {
        if ($this->favedBy->removeElement($favedBy)) {
            $favedBy->removeFavPost($this);
        }

        return $this;
    }



}
