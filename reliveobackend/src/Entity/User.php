<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get",
        "post",
        //"delete" => ["security" => "is_granted('ROLE_USER','ROLE_ADMINISTRATEUR')"],
        //"put" => ["security" => "is_granted('ROLE_ADMINISTRATEUR')"],
        //"patch" => ["security" => "is_granted('ROLE_USER','ROLE_ADMINISTRATEUR')"],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'roles' => 'exact', 'email' => 'exact', 'username' => 'exact', 'password' => 'exact', 'posts' => 'partial', 'favPosts' => 'partial' ])]
class User implements UserInterface, PasswordAuthenticatedUserInterface, TwoFactorInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private $email;

    #[ORM\Column(type: 'string')]
    private $username;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;
    #[ORM\Column(name: "googleAuthenticatorSecret", type: 'string', nullable: true)]
    private ?string $googleAuthenticatorSecret;

    #[ORM\Column(type: 'string', length: 255)]
    private $photo;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $streampassword;

    #[ORM\OneToOne(inversedBy: 'user', targetEntity: Streamer::class, cascade: ['persist', 'remove'])]
    private $streamer;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Post::class, orphanRemoval: true)]
    private $posts;

    #[ORM\ManyToMany(targetEntity: Post::class, inversedBy: 'favedBy')]
    private $favPosts;

    public function __construct()
    {
        $this->favPosts = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * @return array
     *
     * @see UserInterface
     * added to JWTManager.php from lexik/jwt-authentication-bundle/Services/ l.86 - $jwtCreatedEvent = new JWTCreatedEvent(array_merge($user->getCustomPayload(),$payload), $user);
     *
     * N.B. : fait en "semi urgence" en fin de projet dans le but de personnaliser le payload. Nous éviterons à l'avenir de passer par des packages pour gérer le JWT et ferons des helpers à la main.
     */
    public function getCustomPayload() :array
    {
        return ["photo" => $this->getPhoto(), "email" => $this->getEmail(),"userid" => $this->getId()];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set the value of username
     */
    public function getUsername() :string
    {
        return $this->username;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function isGoogleAuthenticatorEnabled(): bool
    {
        return null !== $this->googleAuthenticatorSecret;
    }

    public function getGoogleAuthenticatorUsername(): string
    {
        return $this->username;
    }

    public function getGoogleAuthenticatorSecret(): ?string
    {
        return $this->googleAuthenticatorSecret;
    }

    public function setGoogleAuthenticatorSecret(?string $googleAuthenticatorSecret): void
    {
        $this->googleAuthenticatorSecret = $googleAuthenticatorSecret;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getStreampassword(): ?string
    {
        return $this->streampassword;
    }

    public function setStreampassword(?string $streampassword): self
    {
        $this->streampassword = $streampassword;

        return $this;
    }

    public function getStreamer(): ?Streamer
    {
        return $this->streamer;
    }

    public function setStreamer(?Streamer $streamer): self
    {
        $this->streamer = $streamer;

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setAuthor($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            // set the owning side to null (unless already changed)
            if ($post->getAuthor() === $this) {
                $post->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getFavPosts(): Collection
    {
        return $this->favPosts;
    }

    public function addFavPost(Post $favPost): self
    {
        if (!$this->favPosts->contains($favPost)) {
            $this->favPosts[] = $favPost;
        }

        return $this;
    }

    public function removeFavPost(Post $favPost): self
    {
        $this->favPosts->removeElement($favPost);

        return $this;
    }
}
