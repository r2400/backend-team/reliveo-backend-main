<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

#[ORM\Entity(repositoryClass: EventRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get","post",
        //"post" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_ADMINISTRATEUR')"],
        //"delete" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_ADMINISTRATEUR')"],
        //"put" => ["security" => "is_granted('ROLE_ADMINISTRATEUR')"],
        //"patch" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_ADMINISTRATEUR')"],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'title' => 'exact', 'description' => 'partial', 'geolocation' => 'exact', 'qrCode' =>'exact', 'posts' => 'partial', 'streamer' => 'exact'])]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $longitude;

    #[ORM\Column(type: 'string', length: 255)]
    private $latitude;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'datetime')]
    private $dateStart;

    #[ORM\Column(type: 'datetime')]
    private $dateEnd;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $eventStartingHour;

    #[ORM\Column(type: 'array')]
    private $scenes = [];

    #[ORM\Column(type: 'string', length: 255)]
    private $rue;

    #[ORM\Column(type: 'string', length: 255)]
    private $codepostal;

    #[ORM\Column(type: 'string', length: 255)]
    private $ville;

    #[ORM\OneToMany(mappedBy: 'event', targetEntity: Post::class, orphanRemoval: true)]
    private $posts;

    #[ORM\ManyToOne(targetEntity: Streamer::class, inversedBy: 'events')]
    #[ORM\JoinColumn(nullable: false)]
    private $streamer;

    #[ORM\Column(type: 'string', length: 255)]
    private $photo;

    #[ORM\Column(type: 'string', length: 255)]
    private $qrcode;

    #[ORM\Column(type: 'string', length: 255)]
    private $banner;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getEventStartingHour(): ?\DateTimeInterface
    {
        return $this->eventStartingHour;
    }

    public function setEventStartingHour(?\DateTimeInterface $eventStartingHour): self
    {
        $this->eventStartingHour = $eventStartingHour;

        return $this;
    }

    public function getScenes(): ?array
    {
        return $this->scenes;
    }

    public function setScenes(array $scenes): self
    {
        $this->scenes = $scenes;

        return $this;
    }

    public function getrue(): ?string
    {
        return $this->rue;
    }

    public function setrue(string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    public function setCodepostal(string $codepostal): self
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setEvent($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            // set the owning side to null (unless already changed)
            if ($post->getEvent() === $this) {
                $post->setEvent(null);
            }
        }

        return $this;
    }

    public function getStreamer(): ?Streamer
    {
        return $this->streamer;
    }

    public function setStreamer(?Streamer $streamer): self
    {
        $this->streamer = $streamer;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getQrcode(): ?string
    {
        return $this->qrcode;
    }

    public function setQrcode(string $qrcode): self
    {
        $this->qrcode = $qrcode;

        return $this;
    }

    public function getBanner(): ?string
    {
        return $this->banner;
    }

    public function setBanner(string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

}
