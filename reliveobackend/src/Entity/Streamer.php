<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StreamerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

#[ORM\Entity(repositoryClass: StreamerRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get","post",
        //"post" => ["security" => "is_granted('ROLE_USER','ROLE_ADMINISTRATEUR')"],
        //"delete" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_ADMINISTRATEUR')"],
        //"put" => ["security" => "is_granted('ROLE_ADMINISTRATEUR')"],
        //"patch" => ["security" => "is_granted('ROLE_DIFFUSEUR','ROLE_ADMINISTRATEUR')"],
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['id' => 'exact', 'user' => 'exact', 'events' => 'partial'])]
class Streamer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text', nullable: true)]
    private $streamerStatus;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $liveStatus;

    #[ORM\Column(type:'string', length: 255)]
    private $eventsType;

    #[ORM\Column(type: 'json', nullable: true)]
    private $musicalGenres = [];

    #[ORM\Column(type: 'text', nullable: true)]
    private $streamerWebsite;

    #[ORM\Column(type: 'text')]
    private $streamerDescription;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'streamers')]
    private $streamerId;

    #[ORM\OneToOne(mappedBy: 'streamer', targetEntity: User::class, cascade: ['persist', 'remove'])]
    private $user;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $streamUrl;

    #[ORM\OneToMany(mappedBy: 'streamer', targetEntity: Event::class, orphanRemoval: true)]
    private $events;

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreamerStatus(): ?string
    {
        return $this->streamerStatus;
    }

    public function setStreamerStatus(?string $streamerStatus): self
    {
        $this->streamerStatus = $streamerStatus;

        return $this;
    }

    public function isLiveStatus(): ?bool
    {
        return $this->liveStatus;
    }

    public function setLiveStatus(?bool $liveStatus): self
    {
        $this->liveStatus = $liveStatus;

        return $this;
    }

    public function getEventsType(): ?string
    {
        return $this->eventsType;
    }

    public function setEventsType(string $eventsType): self
    {
        $this->eventsType = $eventsType;

        return $this;
    }

    public function getMusicalGenres(): ?array
    {
        return $this->musicalGenres;
    }

    public function setMusicalGenres(?array $musicalGenres): self
    {
        $this->musicalGenres = $musicalGenres;

        return $this;
    }

    public function getStreamerWebsite(): ?string
    {
        return $this->streamerWebsite;
    }

    public function setStreamerWebsite(?string $streamerWebsite): self
    {
        $this->streamerWebsite = $streamerWebsite;

        return $this;
    }

    public function getStreamerDescription(): ?string
    {
        return $this->streamerDescription;
    }

    public function setStreamerDescription(string $streamerDescription): self
    {
        $this->streamerDescription = $streamerDescription;

        return $this;
    }

    public function getStreamUrl(): ?string
    {
        return $this->streamUrl;
    }

    public function setStreamUrl(?string $streamUrl): self
    {
        $this->streamUrl = $streamUrl;

        return $this;
    }

    public function getStreamerId(): ?User
    {
        return $this->streamerId;
    }

    public function setStreamerId(?User $streamerId): self
    {
        $this->streamerId = $streamerId;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setStreamer(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getStreamer() !== $this) {
            $user->setStreamer($this);
        }

        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, Event>
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setStreamer($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getStreamer() === $this) {
                $event->setStreamer(null);
            }
        }

        return $this;
    }



}
