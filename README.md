# Reliveo

Projet fil rouge HETIC W2  
*Description du projet à écrire plus tard...*

## Setup

Pour installer et lancer le projet:
```
docker-compose up -d
```

Penser récupérer vendor :

```
cd /reliveobackend/
composer install
```

De retour à la racine du repo.
Pour accéder au shell unix:

```
docker-compose exec php sh
```

Pour démarrer le serveur symfony:

```
symfony server:start
```



## Stack

La stack actuelle du projet:

- php:8.1.5-fpm-alpine3.15
- nginx:1.21.6-alpine


# APPELS RELIVEO API
## EVENT
### GET - http://reliveoapi.com/api/events
Usage : Récupérer tous les événements
**REPONSE**
```json
[
  {
    "id": 0,
    "longitude": "string",
    "latitude": "string",
    "name": "string",
    "description": "string",
    "dateStart": "2022-07-10T07:17:07.058Z",
    "dateEnd": "2022-07-10T07:17:07.058Z",
    "eventStartingHour": "2022-07-10T07:17:07.058Z",
    "scenes": [
      "string"
    ],
    "rue": "string",
    "codepostal": "string",
    "ville": "string",
    "posts": [
      "string"
    ],
    "streamer": "string"
  }
]
```
**EXCEPTIONS**
**200** : Event Collection

---
<br>

### POST - http://reliveoapi.com/api/events
Usage : Créer un événement
**REQUETE**
 ```json
{
  "longitude": "string", 
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:18:32.314Z",
  "dateEnd": "2022-07-10T07:18:32.314Z",
  "eventStartingHour": "2022-07-10T07:18:32.314Z", *OPTIONNEL*
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [ *OPTIONNEL*
    "string"
  ],
  "streamer": "string"  *OPTIONNEL*
}
```
**REPONSE**
```json
{
  "id": 0,
  "longitude": "string",
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:18:32.316Z",
  "dateEnd": "2022-07-10T07:18:32.316Z",
  "eventStartingHour": "2022-07-10T07:18:32.316Z",
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [
    "string"
  ],
  "streamer": "string"
}
```

**EXCEPTIONS**
**201** : Event resource created
**400** : Invalid Input
**422** : Unprocessable entity

---
<br>

### GET- http://reliveoapi.com/api/events/{id}
Usage : Récupérer un événement via son ID
**REPONSE**
```json
{
  "id": 0,
  "longitude": "string",
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:22:53.709Z",
  "dateEnd": "2022-07-10T07:22:53.709Z",
  "eventStartingHour": "2022-07-10T07:22:53.709Z",
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [
    "string"
  ],
  "streamer": "string"
}
```

**EXCEPTIONS**
**200** : Event Ressource
**404** : Ressource not found

---
<br>

### PUT - http://reliveoapi.com/api/events/{id}
Usage : Remplace un événement via son ID
**REQUETE**
```json
{
  "longitude": "string",
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:28:17.302Z",
  "dateEnd": "2022-07-10T07:28:17.302Z",
  "eventStartingHour": "2022-07-10T07:28:17.302Z", *OPTIONNEL*
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [ *OPTIONNEL*
    "string"
  ],
  "streamer": "string"  *OPTIONNEL*
}
```
**REPONSE**
```json
{
  "id": 0,
  "longitude": "string",
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:28:17.303Z",
  "dateEnd": "2022-07-10T07:28:17.303Z",
  "eventStartingHour": "2022-07-10T07:28:17.303Z",
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [
    "string"
  ],
  "streamer": "string"
}
```

**EXCEPTIONS**
**200** : Event Ressource Updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity

---
<br>

### DELETE - http://reliveoapi.com/api/events/{id}
Usage : Supprime un événement via son ID

**EXCEPTIONS**
**204** : Event ressource deleted
**404** : Ressource not found

---
<br>

### PATCH - http://reliveoapi.com/api/events/{id}
Usage : Met a jour un événement via son ID
Content-Type : application/merge-patch+json
**REQUETE**
```json
{
  "longitude": "string",
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:32:25.231Z",
  "dateEnd": "2022-07-10T07:32:25.231Z",
  "eventStartingHour": "2022-07-10T07:32:25.231Z", *OPTIONNEL*
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [ *OPTIONNEL*
    "string"
  ],
  "streamer": "string" *OPTIONNEL*
}
```
**REPONSE**
```json
{
  "id": 0,
  "longitude": "string",
  "latitude": "string",
  "name": "string",
  "description": "string",
  "dateStart": "2022-07-10T07:32:25.232Z",
  "dateEnd": "2022-07-10T07:32:25.232Z",
  "eventStartingHour": "2022-07-10T07:32:25.232Z",
  "scenes": [
    "string"
  ],
  "rue": "string",
  "codepostal": "string",
  "ville": "string",
  "posts": [
    "string"
  ],
  "streamer": "string"
}
```
**EXCEPTIONS**
**200** : Event ressource updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity


## POST
### GET - http://reliveoapi.com/api/posts
Usage : Récupérer tous les posts
**REPONSE**
```json
[
  {
    "id": 0,
    "author": "string",
    "event": "string",
    "videoUrl": "string",
    "timestampStart": "2022-07-10T07:34:59.845Z",
    "timestampEnd": "2022-07-10T07:34:59.845Z"
  }
]
```
**EXCEPTIONS**
**200** : Post collection

---
<br>

### POST - http://reliveoapi.com/api/posts
Usage : Créer un post
**REQUETE**
```json
{
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:38:41.572Z",
  "timestampEnd": "2022-07-10T07:38:41.572Z"
}
```
**REPONSE**
```json
{
  "id": 0,
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:38:41.572Z",
  "timestampEnd": "2022-07-10T07:38:41.572Z"
}
```

**EXCEPTIONS**
**201** : Post resource created
**400** : Invalid Input
**422** : Unprocessable entity

---
<br>

### GET- http://reliveoapi.com/api/posts/{id}
Usage : Récupérer un post via son ID
**REPONSE**
```json
{
  "id": 0,
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:40:53.247Z",
  "timestampEnd": "2022-07-10T07:40:53.247Z"
}
```

**EXCEPTIONS**
**200** : Post Ressource
**404** : Ressource not found

---
<br>

### PUT - http://reliveoapi.com/api/posts/{id}
Usage : Remplace un post via son ID
**REQUETE**
```json
{
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:41:51.890Z",
  "timestampEnd": "2022-07-10T07:41:51.890Z"
}
```
**REPONSE**
```json
{
  "id": 0,
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:41:51.908Z",
  "timestampEnd": "2022-07-10T07:41:51.908Z"
}
```

**EXCEPTIONS**
**200** : Post Ressource Updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity

---
<br>

### DELETE - http://reliveoapi.com/api/posts/{id}
Usage : Supprime un post via son ID

**EXCEPTIONS**
**204** : Post ressource deleted
**404** : Ressource not found

---
<br>

### PATCH - http://reliveoapi.com/api/events/{id}
Usage : Met a jour un événement via son ID
Content-Type : application/merge-patch+json
**REQUETE**
```json
{
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:43:02.289Z",
  "timestampEnd": "2022-07-10T07:43:02.289Z"
}
```
**REPONSE**
```json
{
  "id": 0,
  "author": "string",
  "event": "string",
  "videoUrl": "string",
  "timestampStart": "2022-07-10T07:43:02.310Z",
  "timestampEnd": "2022-07-10T07:43:02.310Z"
}
```
**EXCEPTIONS**
**200** : Post resource updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity


## STREAMER
### GET - http://reliveoapi.com/api/streamers
Usage : Récupérer tous les diffuseurs
**REPONSE**
```json
[
  {
    "id": 0,
    "streamerStatus": "string",
    "liveStatus": true,
    "eventsType": "string",
    "musicalGenres": [
      "string"
    ],
    "streamerWebsite": "string",
    "streamerDescription": "string",
    "streamerId": "string",
    "user": "string",
    "streamUrl": "string",
    "events": [
      "string"
    ]
  }
]
```
**EXCEPTIONS**
**200** : Streamer collection

---
<br>

### POST - http://reliveoapi.com/api/streamers
Usage : Créer un diffuseur
**REQUETE**
```json
{
  "streamerStatus": "string",  *OPTIONNEL*
  "liveStatus": true,  
  "eventsType": "string",
  "musicalGenres": [  
    "string"
  ],
  "streamerWebsite": "string",  
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string", 
  "events": [  *OPTIONNEL*
    "string"
  ]
}
```
**REPONSE**
```json
{
  "id": 0,
  "streamerStatus": "string",
  "liveStatus": true,
  "eventsType": "string",
  "musicalGenres": [
    "string"
  ],
  "streamerWebsite": "string",
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string",
  "events": [
    "string"
  ]
}
```

**EXCEPTIONS**
**201** : Streamer resource created
**400** : Invalid Input
**422** : Unprocessable entity

---
<br>

### GET- http://reliveoapi.com/api/streamers/{id}
Usage : Récupérer un streamer via son ID
**REPONSE**
```json
{
  "id": 0,
  "streamerStatus": "string",
  "liveStatus": true,
  "eventsType": "string",
  "musicalGenres": [
    "string"
  ],
  "streamerWebsite": "string",
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string",
  "events": [
    "string"
  ]
}
```

**EXCEPTIONS**
**200** : Streamer Ressource
**404** : Ressource not found

---
<br>

### PUT - http://reliveoapi.com/api/streamers/{id}
Usage : Remplace un diffuseur via son ID
**REQUETE**
```json
{
  "streamerStatus": "string",  *OPTIONNEL*
  "liveStatus": true,
  "eventsType": "string",
  "musicalGenres": [
    "string"
  ],
  "streamerWebsite": "string",
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string",
  "events": [  *OPTIONNEL*
    "string"
  ]
}
```
**REPONSE**
```json
{
  "id": 0,
  "streamerStatus": "string",
  "liveStatus": true,
  "eventsType": "string",
  "musicalGenres": [
    "string"
  ],
  "streamerWebsite": "string",
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string",
  "events": [
    "string"
  ]
}
```

**EXCEPTIONS**
**200** : Streamer ressource updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity

---
<br>

### DELETE - http://reliveoapi.com/api/streamers/{id}
Usage : Supprime un diffuseur via son ID

**EXCEPTIONS**
**204** : Streamer ressource deleted
**404** : Ressource not found

---
<br>

### PATCH - http://reliveoapi.com/api/streamer/{id}
Usage : Met a jour un diffuseur via son ID
Content-Type : application/merge-patch+json
**REQUETE**
```json
{
  "streamerStatus": "string", *OPTIONNEL*
  "liveStatus": true,
  "eventsType": "string",
  "musicalGenres": [
    "string"
  ],
  "streamerWebsite": "string",
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string",
  "events": [ *OPTIONNEL*
    "string"
  ]
}
```
**REPONSE**
```json
{
  "id": 0,
  "streamerStatus": "string",
  "liveStatus": true,
  "eventsType": "string",
  "musicalGenres": [
    "string"
  ],
  "streamerWebsite": "string",
  "streamerDescription": "string",
  "streamerId": "string",
  "user": "string",
  "streamUrl": "string",
  "events": [
    "string"
  ]
}
```
**EXCEPTIONS**
**200** : Streamer resource updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity


## USER
### GET - http://reliveoapi.com/api/users
Usage : Récupérer tous les utilisateurs
**REPONSE**
```json
[
  {
    "id": 0,
    "email": "string",
    "username": "string",
    "roles": [
      "string"
    ],
    "password": "string",
    "googleAuthenticatorSecret": "string",
    "photo": "string",
    "streampassword": "string",
    "streamer": "string",
    "posts": [
      "string"
    ],
    "customPayload": [
      "string"
    ],
    "userIdentifier": "string",
    "googleAuthenticatorEnabled": true,
    "googleAuthenticatorUsername": "string"
  }
]
```
**EXCEPTIONS**
**200** : User collection

---
<br>

### POST - http://reliveoapi.com/api/users
Usage : Créer un utilisateur
**REQUETE**
```json
{
  "email": "string",
  "username": "string", 
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string", *OPTIONNEL*
  "photo": "string", 
  "streampassword": "string", *OPTIONNEL*
  "streamer": "string",
  "posts": [  *OPTIONNEL*
    "string"
  ]
}
```
**REPONSE**
```json
{
  "id": 0,
  "email": "string",
  "username": "string",
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string",
  "photo": "string",
  "streampassword": "string",
  "streamer": "string",
  "posts": [
    "string"
  ],
  "customPayload": [
    "string"
  ],
  "userIdentifier": "string",
  "googleAuthenticatorEnabled": true,
  "googleAuthenticatorUsername": "string"
}
```

**EXCEPTIONS**
**201** : User resource created
**400** : Invalid Input
**422** : Unprocessable entity

---
<br>

### GET- http://reliveoapi.com/api/users/{id}
Usage : Récupérer un utilisateurs via son ID
**REPONSE**
```json
{
  "id": 0,
  "email": "string",
  "username": "string",
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string",
  "photo": "string",
  "streampassword": "string",
  "streamer": "string",
  "posts": [
    "string"
  ],
  "customPayload": [
    "string"
  ],
  "userIdentifier": "string",
  "googleAuthenticatorEnabled": true,
  "googleAuthenticatorUsername": "string"
}
```

**EXCEPTIONS**
**200** : User Ressource
**404** : Ressource not found

---
<br>

### PUT - http://reliveoapi.com/api/users/{id}
Usage : Remplace un utilisateur via son ID
**REQUETE**
```json
{
  "email": "string",
  "username": "string",
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string", *OPTIONNEL*
  "photo": "string",
  "streampassword": "string", *OPTIONNEL*
  "streamer": "string",
  "posts": [ *OPTIONNEL*
    "string"
  ]
}
```
**REPONSE**
```json
{
  "id": 0,
  "email": "string",
  "username": "string",
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string",
  "photo": "string",
  "streampassword": "string",
  "streamer": "string",
  "posts": [
    "string"
  ],
  "customPayload": [
    "string"
  ],
  "userIdentifier": "string",
  "googleAuthenticatorEnabled": true,
  "googleAuthenticatorUsername": "string"
}
```

**EXCEPTIONS**
**200** : User ressource updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity

---
<br>

### DELETE - http://reliveoapi.com/api/users/{id}
Usage : Supprime un utilisateurs via son ID

**EXCEPTIONS**
**204** : User ressource deleted
**404** : Ressource not found

---
<br>

### PATCH - http://reliveoapi.com/api/users/{id}
Usage : Met a jour un utilisateur via son ID
Content-Type : application/merge-patch+json
**REQUETE**
```json
{
  "email": "string",
  "username": "string",
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string", *OPTIONNEL*
  "photo": "string",
  "streampassword": "string", *OPTIONNEL*
  "streamer": "string",
  "posts": [ *OPTIONNEL*
    "string"
  ]
}
```
**REPONSE**
```json
{
  "id": 0,
  "email": "string",
  "username": "string",
  "roles": [
    "string"
  ],
  "password": "string",
  "googleAuthenticatorSecret": "string",
  "photo": "string",
  "streampassword": "string",
  "streamer": "string",
  "posts": [
    "string"
  ],
  "customPayload": [
    "string"
  ],
  "userIdentifier": "string",
  "googleAuthenticatorEnabled": true,
  "googleAuthenticatorUsername": "string"
}
```
**EXCEPTIONS**
**200** : User resource updated
**400** : Invalid Input
**404** : Ressource not found
**422** : Unprocessable entity

##AUTHENTICATION_TOKEN / Login / Refesh token
### POST - http://reliveoapi.com/authentication_token
Usage : Connexion à l'app, obtention du JWT Token
Content-Type: application/json
